var host = "192.168.2.29"
var host = "203.194.112.238"
    //var host = window.location.hostname;

var port = 8083;
var userId = "das";
var passwordId = "mgi2023";
var coba = "80deg";
var grafikData;
var datacoba;
// /startConnect()

function startConnect() {
    clientID = "dms-" + parseInt(Math.random() * 100);
    client = new Paho.MQTT.Client(host, Number(port), clientID);
    $("#mdbusChartIA").attr("data-ratio", coba);
    client.onConnectionLost = onConnectionLost;
    client.onMessageArrived = onMessageArrived;

    client.connect({


        // userName: userId,
        //password: passwordId,
        onSuccess: onConnect
    });
}

function onConnect() {
    topic = "DMS/#";
    client.subscribe(topic);
    console.log("connected")
}

function onConnectionLost(responseObject) {
    startConnect()
    console.log("disconnect")
        //alert("disconnect")
        // document.getElementById("messages").innerHTML +=
        //     "<span> ERROR: Connection is lost.</span><br>";
        // if (responseObject != 0) {
        //     document.getElementById("messages").innerHTML +=
        //         "<span> ERROR:" + responseObject.errorMessage + "</span><br>";
        // }
}


function timeConverter(UNIX_timestamp) {
    let date = new Date(parseInt(UNIX_timestamp))
    var date_string = date.toLocaleString('en-GB');

    return date_string;
}

function onMessageArrived(message) {
    //console.log(message.payloadString)
    //alert("disconnect")
    dataMon = JSON.parse($('#monitorData').text())
    parsemsg = JSON.parse(message.payloadString)
    parseDest = message.destinationName.split("/")
    machine_code = (parseDest[1])
    id_device = (parseDest[3])
    item_id = (parseDest[4])
    alias = parsemsg.alias

    if (parsemsg.dataType == "float" || parsemsg.dataType == "integer") {
        let val = parseFloat(parsemsg.val)
            // try {
            //     if (val > 0) {
            //         console.log(val)
            //             //$(`.icon_${buf[i].machine_code}_${buf[i].id_device}_${buf[i].alias}`).removeClass("text-white")
            //         $(`.icon_${buf[i].machine_code}_${buf[i].id_device}_${buf[i].alias}`).addClass("text-yellow")
            //             // console.log(`ic_${buf[i].machine_code}_${buf[i].id_device}_${buf[i].alias}`)
            //     } else if (val == 0) {
            //         // $(`.icon_${buf[i].machine_code}_${buf[i].id_device}_${buf[i].alias}`).removeClass("text-yellow")
            //         $(`.icon_${buf[i].machine_code}_${buf[i].id_device}_${buf[i].alias}`).addClass("text-white")
            //     }
            // } catch (err) {}
        if (parsemsg.dataType == "float") {
            $(`#dtx_${machine_code}_${id_device}_${alias}`).text(val.toFixed(3))
        } else {
            val = parseInt(parsemsg.val)
                //
            $(`#dtx_${machine_code}_${id_device}_${alias}`).text(val)
        }


    } else if (parsemsg.dataType.toLowerCase() == "boolean") {
        let val = parsemsg.val
        $(`#dtx_${machine_code}_${id_device}_${alias}`).text(parsemsg.val.toUpperCase())
        if (val.toLowerCase() == "true") {
            $(`.icon_${machine_code}_${id_device}_${alias}`).removeClass("bg-gray")
            $(`.icon_${machine_code}_${id_device}_${alias}`).addClass("bg-red")
                // console.log("hidup")
            $(`#dt_${machine_code}_${id_device}_${alias}`).removeClass("l-second")
            $(`#dt_${machine_code}_${id_device}_${alias}`).addClass("l-red")

        } else {
            $(`.icon_${machine_code}_${id_device}_${alias}`).removeClass("bg-red")
            $(`.icon_${machine_code}_${id_device}_${alias}`).addClass("bg-gray")
            $(`#dt_${machine_code}_${id_device}_${alias}`).removeClass("l-red")
            $(`#dt_${machine_code}_${id_device}_${alias}`).addClass("l-second")
                //console.log("mati")
        }
    } else if (parsemsg.dataType == "visible-string") {

        let val = parsemsg.val
        $(`#dtx_${machine_code}_${id_device}_${alias}`).text(val)

    } else if (parsemsg.dataType == "utc-time") {
        $(`#dtx_${machine_code}_${id_device}_${alias}`).removeClass("info-box-number")
        let val = timeConverter(parsemsg.val)
        $(`#dtx_${machine_code}_${id_device}_${alias}`).text(val)
    }
    try {
        if (parsemsg.dataType == "float" || parsemsg.dataType == "integer") {
            localStorage.setItem(`${machine_code}_${id_device}_${alias}`, message.payloadString);
            arr = localStorage.getItem(`arr_${machine_code}_${id_device}_${alias}`)
            try {
                JsonArr = JSON.parse(arr)
                if (JsonArr.length < 30) {
                    JsonArr.shift()
                    JsonArr.push(parsemsg.val)
                    localStorage.setItem(`arr_${machine_code}_${id_device}_${alias}`, JSON.stringify(JsonArr))
                }
            } catch (error) {
                localStorage.setItem(`arr_${machine_code}_${id_device}_${alias}`, JSON.stringify([0]))
            }
        }
    } catch (err) {
        console.log(err)
    }

}

function startDisconnect() {
    client.disconnect();
    document.getElementById("messages").innerHTML +=
        "<span> Disconnected. </span><br>";
}

function publishMessage() {
    msg = document.getElementById("Message").value;
    topic = document.getElementById("topic_p").value;

    Message = new Paho.MQTT.Message(msg);
    Message.destinationName = topic;

    client.send(Message);
    document.getElementById("messages").innerHTML +=
        "<span> Message to topic " + topic + " is sent </span><br>";
}
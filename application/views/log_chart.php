
<script src="https://cdn.amcharts.com/lib/4/core.js"></script>
<script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
<script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>

<!-- t?code=${param[0]}&device=${param[1]}&alias=${ -->

 
<select >
  <option value="now"  <?php if($_GET["tipe"]=="now"){echo 'selected';}?>>Current Date</option>
  <option value="week" <?php if($_GET["tipe"]=="week"){echo 'selected';}?>>Last Week</option>
  <option value="month" <?php if($_GET["tipe"]=="month"){echo 'selected';}?>>Last Month</option>
 </select>
<div class="card card-bordered">
    <p hidden id="log"><?php echo json_encode($log)?></p>
    <div class="card-body">
        <div id="kt_amcharts_2" style="height: 500px;"></div>
    </div>
</div>

<script>
am4core.ready(function () {

// Themes begin
am4core.useTheme(am4themes_animated);
// Themes end
var data = [];
$('select').on('change', function() {
  let code =$('#code').text()
  let device =$('#device').text()
  let alias =$('#alias').text()
  $("#log-body").load(`../../device_relay/log_chart?code=${code}&device=${device}&alias=${alias}&tipe=${this.value}`);
 //  console.log(`../../device_relay/log_chart?code=${code}&device=${device}&alias=${alias}&tipe=${this.value}`)
});

let logshtml = document.getElementById('log').innerHTML
let logs=JSON.parse(logshtml)
for (let i=0;i<logs.length;i++){
    let newDate = Date.parse(new Date(logs[i].tanggal))
    data.push({ date1: newDate, log: logs[i].val });
}

// Create chart
chart = am4core.create('kt_amcharts_2', am4charts.XYChart);

var price1 = 1000, price2 = 1200;
var quantity = 30000;
// for (var i = 0; i < 360; i++) {
//     price1 += Math.round((Math.random() < 0.5 ? 1 : -1) * Math.random() * 100);
//     data.push({ date1: new Date(2015, 0, i), price1: price1 });
// }
// for (var i = 0; i < 360; i++) {
//     price2 += Math.round((Math.random() < 0.5 ? 1 : -1) * Math.random() * 100);
//     data.push({ date2: new Date(2017, 0, i), price2: price2 });
// }
console.log(data)
chart.data = data;

var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
dateAxis.renderer.grid.template.location = 0;
dateAxis.renderer.labels.template.fill = am4core.color('#e59165');


var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
valueAxis.tooltip.disabled = true;
valueAxis.renderer.labels.template.fill = am4core.color('#0000FF');

valueAxis.renderer.minWidth = 60;


var series = chart.series.push(new am4charts.LineSeries());
series.name = 'Data Value';
series.dataFields.dateX = 'date1';
series.dataFields.valueY = 'log';
series.tooltipText = '{valueY.value}';
series.fill = am4core.color('#0000FF');
series.stroke = am4core.color('#0000FF');
series.strokeWidth = 2;


chart.cursor = new am4charts.XYCursor();

var scrollbarX = new am4charts.XYChartScrollbar();
scrollbarX.series.push(series);
chart.scrollbarX = scrollbarX;

series.tensionX=1

series.tensionY=1
chart.legend = new am4charts.Legend();
chart.legend.parent = chart.plotContainer;
chart.legend.zIndex = 100;


}); // end am4core.ready()
</script>